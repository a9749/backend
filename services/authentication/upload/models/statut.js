let {Sequelize, DataTypes} = require('sequelize');
const db = require('/opt/nodejs/database')

var statut = db.define('Statut', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'statut_id'
    },
    val_statut: {
        type: DataTypes.STRING,
        field: 'val_statut'
    },
    argument: {
        type: DataTypes.STRING,
        field: 'argument'
    }
}, {
    freezeTableName: true,
});
module.exports = statut;