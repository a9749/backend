let {Sequelize, DataTypes} = require('sequelize');
const db = require('/opt/nodejs/database')
const user = require('./user')

var locataire = db.define('Locataire', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'locataire_id',
        references: {
            model: user,
            column: 'utilisateur_id'
        }
    },
    adresse_locataire: {
        type: DataTypes.STRING,
        field: 'adresse_locataire'
    },
    photo: {
        type: DataTypes.STRING,
        field: 'photo'
    },
    piece_identite: {
        type: DataTypes.STRING,
        field: 'piece_identite'
    }
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports = locataire;