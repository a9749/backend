const UtilisateurModel = require('models/user');
const LocataireModel = require('models/locataire');
const aws = require('aws-sdk')
var crypto = require('crypto');
const Busboy = require('busboy');

let response;

let s3 = new aws.S3({
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY
})

function buildResponse(responseCode, body) {
    return {
        'statusCode': responseCode,
        'body': JSON.stringify(body)
    }
}

const getContentType = (event) => {
    const contentType = event.headers['content-type']
    if (!contentType) {
        return event.headers['Content-Type'];
    }
    return contentType;
};

const parser = (event) => new Promise((resolve, reject) => {
    const busboy = Busboy({
        headers: {
            'content-type': getContentType(event)
        }
    });

    const result = {};

    busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
        file.on('data', data => {
            result.file = data;
        });

        file.on('end', () => {
            result.filename = filename;
            result.contentType = mimetype;
        });
    });

    busboy.on('field', (fieldname, value) => {

        result[fieldname] = value;
    });

    busboy.on('error', error => reject(error));
    busboy.on('finish', () => {
        event.body = result;
        resolve(event);
    });

    busboy.write(event.body, event.isBase64Encoded ? 'base64' : 'binary');
    busboy.end();
});

exports.lambdaHandler = async (event, context) => {

    let splits;
    let type;

    try {
        console.log(event)
        // Parsing the binary file
        await parser(event)

        var name = (new Date()).toString();
        var hash = crypto.createHash('md5').update(name).digest('hex');

        // Get file type
        type = event.body.filename.mimeType
        splits = type.split('/') // e.g: image/jpeg
        type = splits[1]

        console.log("File type is : " + type);
        const params = {
            Bucket: process.env.UPLOAD_BUCKET_NAME, // pass your bucket name
            Key: hash + "." + type, // To guarantee a unique key for the object
            ACL: "public-read",
            Body: event.body.file,
            ContentType: event.headers['content-type']
        };

        const data = await s3.upload(params).promise();

        return buildResponse(200, {
            message: "File uploaded",
            location: data.Location,
            event: JSON.stringify(event)
        })

    } catch (err) {
        console.log(err);
        return buildResponse(500, err);
    }
};
