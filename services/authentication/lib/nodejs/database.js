var Sequelize = require('sequelize');

const host = process.env.PGHOST;
const user = process.env.PGUSER;
const password = process.env.PGPASSWORD;
const database = process.env.PGDATABASE;

var sequelize = new Sequelize(database, user, password, {
    host: host,
    omitNull: true,
    dialect: 'postgres',
});

try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}

module.exports = sequelize;