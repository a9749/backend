let crypto = require('crypto-js/hmac-sha256');

const Utils = {
    setPassword : function (password) {
        // Creating a unique salt for a particular user
        let salt = process.env.PASSWORD_HASH_KEY;

        // Hashing user's salt and password ,
        hash = crypto(password, salt).toString();

        return hash;
    },

    validatePassword: function (password, hashedPassword) {
        // Creating a unique salt for a particular user
        let salt = process.env.PASSWORD_HASH_KEY;

        // Hashing user's salt and password ,
        hash = crypto(password, salt).toString();

        return hashedPassword === hash;
    }
}


module.exports = Utils;