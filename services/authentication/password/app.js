const UtilisateurModel = require('models/user');
const Utils = require('/opt/nodejs/utils');
let jwt = require("jsonwebtoken");

const validator = require('validator')


function buildResponse(responseCode, body, header) {
    if (!header) {
        return {
            'statusCode': responseCode,
            'body': JSON.stringify(body)
        };
    } else {
        return {
            'statusCode': responseCode,
            'headers': header,
            'body': JSON.stringify(body)
        }
    }
}

function validateLogin(body) {
    if (body.email && body.mdp) {
        return validator.isEmail(body.email)
            && validator.isLength(body.mdp, {min: 8})
    } else
        return false;
}


let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 *
 */
exports.lambdaHandler = async (event, context) => {
    let foundUser;

    try {

        const body = JSON.parse(event.body);

        const {id, oldPassword, newPassword} = body;

        foundUser = await UtilisateurModel.findOne({
            where: {
                id: id
            }
        })

        if (!foundUser) {
            return buildResponse(404, {
                message: "User not found"
            })
        }

        console.log("Old password: " + oldPassword)
        console.log("Hashed Old password: " + Utils.setPassword(oldPassword))
        console.log("Password in DB: " + foundUser.mdp)
        if (!Utils.validatePassword(oldPassword, foundUser.mdp)) {
            return buildResponse(401, {
                message: "Wrong user password"
            })
        }

        let mdp = Utils.setPassword(newPassword);
        foundUser.set({
            mdp: mdp
        })
        foundUser.save();
        return buildResponse(200, foundUser);

    } catch (err) {
        console.log(err);
        return buildResponse(500, err);
    }
};
