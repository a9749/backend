const UtilisateurModel = require('models/user');
const AuthModel = require('models/auth');
const ATCModel = require('models/atc');
const LocataireModel = require('models/locataire');
const DecideurModel = require('models/decideur');
const AMModel = require('models/agentMaintenance');
const StatutModel = require('models/statut');

const Utils = require('/opt/nodejs/utils');
let jwt = require("jsonwebtoken");

const validator = require('validator')


function buildResponse(responseCode, body, header) {
    if (!header) {
        return {
            'statusCode': responseCode,
            'body': JSON.stringify(body)
        };
    } else {
        return {
            'statusCode': responseCode,
            'headers': header,
            'body': JSON.stringify(body)
        }
    }
}

function validateLogin(body) {
    if (body.email && body.mdp) {
        return validator.isEmail(body.email)
            && validator.isLength(body.mdp, {min: 8})
    } else
        return false;
}


let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 *
 */
exports.lambdaHandler = async (event, context) => {
    let foundUser;

    try {

        const body = JSON.parse(event.body);

        //Check the route
        const {typeUser} = event.pathParameters;
        switch (typeUser) {
            case "am":
                //validate the payload
                if (!validateLogin(body)) {
                    return buildResponse(400, {
                        message: "Input is not valid"
                    })
                }
                //search for existing user
                foundUser = await UtilisateurModel.findOne({
                    where: {
                        email: body.email,
                        type_utilisateur: "am"
                    }
                });
                if (!foundUser) {
                    return buildResponse(404, {
                        message: "User not found"
                    })
                }

                // if user found: Verify password
                if (!Utils.validatePassword(body.mdp, foundUser.mdp)) {
                    return buildResponse(401, {
                        message: "Wrong password"
                    })
                }
                // issue a token and save it to DB
                access_token = jwt.sign({...foundUser}, process.env.ACCESS_TOKEN_KEY, {
                    expiresIn: '2d',
                    audience: 'am',
                })
                // Setting the header of the response
                header = {
                    Authorization: 'Bearer ' + access_token
                }
                // Removing mdp
                delete foundUser.mdp

                return buildResponse(200, {
                    user: foundUser
                }, header)
                break;

            case "atc":
                //validate the payload
                if (!validateLogin(body)) {
                    return buildResponse(400, {
                        message: "Input is not valid"
                    })
                }
                //search for existing user
                foundUser = await UtilisateurModel.findOne({
                    where: {
                        email: body.email,
                        type_utilisateur: 'atc'
                    }
                })
                if (!foundUser) {
                    return buildResponse(404, {
                        message: "User not found"
                    })
                }

                // if user found: Verify password
                if (!Utils.validatePassword(body.mdp, foundUser.mdp)) {
                    return buildResponse(401, {
                        message: "Wrong password"
                    })
                }
                // issue a token and save it to DB
                access_token = jwt.sign({...foundUser}, process.env.ACCESS_TOKEN_KEY, {
                    expiresIn: '3h',
                    audience: 'atc',
                })
                // Setting the header of the response
                header = {
                    Authorization: 'Bearer ' + access_token,
                }

                // Removing mdp
                delete foundUser.mdp
                return buildResponse(200, {
                    user: foundUser
                }, header)
                break;

            case "decideur":
                //validate the payload
                if (!validateLogin(body)) {
                    return buildResponse(400, {
                        message: "Input is not valid"
                    })
                }
                //search for existing user
                foundUser = await UtilisateurModel.findOne({
                    where: {
                        email: body.email,
                        type_utilisateur: 'decideur'
                    }
                })
                if (!foundUser) {
                    return buildResponse(404, {
                        message: "User not found"
                    })
                }

                // if user found: Verify password
                if (!Utils.validatePassword(body.mdp, foundUser.mdp)) {
                    return buildResponse(401, {
                        message: "Wrong password"
                    })
                }
                // issue a token and save it to DB
                access_token = jwt.sign({
                    ...foundUser
                }, process.env.ACCESS_TOKEN_KEY, {
                    expiresIn: '2d',
                    audience: 'decideur',
                })
                // Setting the header of the response
                header = {
                    Authorization: 'Bearer ' + access_token
                }
                // Removing mdp
                delete foundUser.mdp

                return buildResponse(200, {
                    user: foundUser
                }, header)
                break;

            case "locataire":
                //validate the payload
                if (!validateLogin(body)) {
                    return buildResponse(400, {
                        message: "Input is not valid"
                    })
                }
                //search for existing user
                foundUser = await UtilisateurModel.findOne({
                    where: {
                        email: body.email,
                        type_utilisateur: 'locataire'
                    }
                })
                if (!foundUser) {
                    return buildResponse(404, {
                        message: "User not found"
                    })
                }

                // Getting user status
                let statut = await StatutModel.findOne({
                    where: {
                        id: foundUser.statut_utilisateur
                    }
                })

                // Getting locataire table
                let locataire = await LocataireModel.findOne({
                    where: {
                        id: foundUser.id
                    }
                })

                // if user found: Verify password
                if (!Utils.validatePassword(body.mdp, foundUser.mdp)) {
                    return buildResponse(401, {
                        message: "Wrong password"
                    })
                }
                // issue a token and save it to DB
                access_token = jwt.sign({...foundUser, statut: statut.val_statut}, process.env.ACCESS_TOKEN_KEY, {
                    expiresIn: '2d',
                    audience: 'locataire',
                })
                // Setting the header of the response
                header = {
                    Authorization: 'Bearer ' + access_token
                }

                // Removing mdp
                delete foundUser.mdp

                // Building full user object
                user = {
                    user: foundUser,
                    statut: statut.val_statut,
                    locataire: locataire
                }

                return buildResponse(200, user, header)
        }

    } catch (err) {
        console.log(err);
        return buildResponse(500, err);
    }
};
