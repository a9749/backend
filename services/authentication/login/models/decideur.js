let {Sequelize, DataTypes} = require('sequelize');
const db = require('/opt/nodejs/database')
const user = require('./user')

var decideur = db.define('Decideur', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'decideur_id',
        references: {
            model: user,
            column: 'utilisateur_id'
        }
    },

}, {
    freezeTableName: true,
    timestamps: false // Model tableName will be the same as the model name
});

module.exports = decideur;