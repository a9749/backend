const UtilisateurModel = require('models/user');
const LocataireModel = require('models/locataire');
const StatutModel = require('models/statut');
const Utils = require('/opt/nodejs/utils');
const aws = require('aws-sdk');
const validator = require('validator')

let response;

function buildResponse(responseCode, body) {
    return {
        'statusCode': responseCode,
        'body': JSON.stringify(body)
    }
}

function validateLocataireSignup(body) {
    try {
        return validator.isEmail(body.email)
            && validator.isAlpha(body.nom)
            && validator.isLength(body.nom, {min: 3})
            && validator.isLength(body.prenom, {min: 3})
            && validator.isNumeric(body.num_tel)
            && validator.isLength(body.num_tel, {min: 9})
            && validator.isLength(body.adresse_locataire, {min: 15})
    } catch (e) {
        return false;
    }
}

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */
exports.lambdaHandler = async (event, context) => {

    try {

        const body = JSON.parse(event.body);
        console.log(body)
        //Check the route
        const {typeUser} = event.pathParameters;
        if (typeUser != "locataire")
            return buildResponse(404, {
                message: "User type not known"
            })
        //Validate the payload
        if (!validateLocataireSignup(body)) {
            return buildResponse(400, {
                message: "Wrong input payload"
            })
        }


        // Search if user exists
        const searchUser = await UtilisateurModel.findOne({
            where: {
                email: body.email,
                type_utilisateur: "locataire"
            }
        })
        if (searchUser) {
            return buildResponse(400, {
                message: "User exists already"
            })
        }

        let hashedPassword = Utils.setPassword(body.mdp);
        console.log(hashedPassword)
        delete body.mdp;

        //create un status
        const statut = await StatutModel.create({
            argument: "aucun",
            val_statut: "demandé"
        })

        const user = await UtilisateurModel.create({
            email: body.email,
            prenom: body.prenom,
            nom: body.nom,
            num_tel: body.num_tel,
            mdp: hashedPassword,
            type_utilisateur: "locataire",
            statut_utilisateur: statut.id });

        // Creating a Locataire
        await LocataireModel.create({
            id: user.id,
            adresse_locataire: body.adresse_locataire,
            photo: body.photo, //url to S3
            piece_identite: body.piece_identite, //url to s3
        })

        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: "User created successfully"
            })
        }
    } catch (err) {
        console.log(err);
        return buildResponse(502, err);
    }

    return response
};
