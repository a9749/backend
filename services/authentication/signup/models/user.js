let {Sequelize, DataTypes} = require('sequelize');
const db = require('/opt/nodejs/database')
const statut = require('./statut.js')

var User = db.define('Utilisateur', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'utilisateur_id'
    },
    nom: {
        type: DataTypes.STRING,
        field: 'nom'
    },
    prenom: {
        type: DataTypes.STRING,
        field: 'prenom'
    },
    email: {
        type: DataTypes.STRING,
        field: 'email'
    },
    num_tel: {
        type: DataTypes.STRING,
        field: 'num_tel'
    },
    mdp: {
        type: DataTypes.STRING,
        field: 'mdp'
    },
    type_utilisateur: {
        type: DataTypes.STRING,
        field: 'type_utilisateur'
    },
    statut_utilisateur: {
        type: DataTypes.INTEGER,
        field: 'statut_utilisateur',
        references: {
            model: statut
        }
    }
}, {
    freezeTableName: true, // Model tableName will be the same as the model name
    initialAutoIncrement: 1000
});
module.exports = User;