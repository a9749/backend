const UtilisateurModel = require('models/user');
const LocataireModel = require('models/locataire');
const StatutModel = require('models/statut');
const validator = require('validator');
const fs = require('fs');
const nodemailer = require("nodemailer");
const Utils = require('/opt/nodejs/utils');



let response;

function buildResponse(responseCode, body) {
    return {
        'statusCode': responseCode,
        'body': JSON.stringify(body)
    }
}

function validateLocataireSignup(body) {
    return validator.isEmail(body.email)
        || validator.isAlpha(body.nom)
        || validator.isLength(body.nom, {min: 6})
        || validator.isAlpha(body.prenom)
        || validator.isLength(body.prenom, {min: 3})
        || validator.isNumeric(body.num_tel)
        || validator.isLength(body.num_tel, {min: 9})
        || validator.isLength(body.adresse_locataire, {min: 15})
}

function getEmailBody(name, isAccepted, argument)  {
    let htmlBody;

    if (isAccepted) {

        htmlBody = fs.readFileSync("views/acceptedLocataire.html", 'utf-8');
        htmlBody = htmlBody.replace('*|NAME|*', name)
    } else {

        htmlBody = fs.readFileSync("views/RefusedLocataire.html", 'utf-8');
        htmlBody = htmlBody.replace('*|NAME|*', name)
        htmlBody = htmlBody.replace('*|ARG|*', argument)
    }

    return htmlBody;
}

async function sendEmail(to, subject, body) {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        auth: {
            user: 'hm_benmoussat@esi.dz',
            pass: process.env.EMAIL_PASSWORD,
        },
        tls: {
            ciphers:'SSLv3',
            rejectUnauthorized: false,
        }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"Autorun @ Futura " <foo@example.com>', // sender address
        to: to, // could be a list of receivers
        subject: subject, // Subject line
        html: body, // html body
    });

    console.log("Message sent: %s", info.messageId);

}

exports.lambdaHandler = async (event, context) => {
    const {method} = event.requestContext.http;

    // Defining models associations
    UtilisateurModel.hasOne(LocataireModel, {
        foreignKey: 'locataire_id'
    });

    UtilisateurModel.belongsTo(StatutModel, {
        foreignKey: 'statut_utilisateur'
    });

    let body;
    let myUser = null;
    let userStaut = null;

    try {
        switch (method) {
            case "GET":
                let {id} = event.pathParameters;
                if (id) {// Return a certain locataire

                    const user = await UtilisateurModel.findOne({
                        where: {
                            id: id,
                            type_utilisateur: "locataire"
                        },
                        include: [LocataireModel, StatutModel]
                    });
                    if (!user) {
                        return buildResponse(404, {
                            message: "User not found"
                        })
                    }
                    return buildResponse(200, {
                        user: user,
                    });
                } else {
                    // return all locataires
                    const users = await UtilisateurModel.findAll({where: {
                            type_utilisateur: "locataire"
                        },include: [LocataireModel, StatutModel]});
                    if (!users) {
                        return buildResponse(404, {
                            message: "User not found"
                        })
                    }
                    console.log(users)
                    return buildResponse(200, users);
                }
                break;
            case "POST":
                // Validate body
                body = JSON.parse(event.body);
                validateLocataireSignup(body);

                // Getting user
                let user =  await UtilisateurModel.findOne({
                    where: {
                        email: body.email,
                        type_utilisateur: "locataire"
                    },
                    include: [LocataireModel, StatutModel]
                })
                if (user) {
                    return buildResponse(403, {
                        message: "Locataire exists already"
                    })
                }
                //create un status
                userStatut = await StatutModel.create({
                    argument: "aucun",
                    val_statut: "demandé"
                })

                user = await UtilisateurModel.create({
                    ...body,
                    mdp: hashedPassword,
                    type_utilisateur: "locataire",
                    statut_utilisateur: userStatut.id });

                // Creating a Locataire
                await LocataireModel.create({
                    id: user.id,
                    adresse_locataire: body.adresse_locataire,
                    photo: "", //url to S3
                    piece_identite: "", //url to s3
                })

                return buildResponse(200, {
                    message: "User created successfully",
                    user: user
                })
            case "PUT":
                body = JSON.parse(event.body)
                // Validate body
                // TODO make locataire update validator
                // if (!validateLocataireSignup(body)) {
                //     return buildResponse(400, {
                //         message: "Wrong input"
                //     })
                // }

                let idLocataire = event.pathParameters.id;

                let putUser =  await UtilisateurModel.findOne({
                    where: {
                        id: idLocataire,
                        type_utilisateur: "locataire"
                    },
                    include: [LocataireModel, StatutModel]
                })

                let putLocataire = await LocataireModel.findOne({
                    where: {
                        id: idLocataire
                    }
                })

                // Check if user exists
                if (!putUser && !putLocataire) {
                    return buildResponse(404, {
                        message: "Locataire non existant"
                    })
                }


                putUser.set({
                    prenom: body.prenom,
                    //TODO: add password change
                    // mdp: Utils.setPassword(body.mdp), //Hach the password
                    nom: body.nom,
                    num_tel: body.num_tel
                });
                putUser.save();

                putLocataire.set({
                    ...body.Locataire
                })
                putLocataire.save()
                return buildResponse(200, {
                    message:" User updated",
                    user: putUser
                })
                break;
            case "DELETE":
                let myId = event.pathParameters.id;
                if (myId) {

                    const user = await UtilisateurModel.findOne({
                        where: {
                            id: myId,
                            type_utilisateur: "locataire"
                        },
                        include: [LocataireModel, StatutModel]
                    });
                    if (!user) {
                        return buildResponse(404, {
                            message: "User not found"
                        })
                    }
                    await user.destroy({
                        include: [LocataireModel, StatutModel]
                    });
                    return buildResponse(200, {
                        message: "User Deleted"
                    })
                }
                break;
            case "PATCH":
                let {status} = event.queryStringParameters;
                locataireId = event.pathParameters.id



                //check if user exists
                myUser = await UtilisateurModel.findOne({
                    where: {
                        id: locataireId,
                    },
                    include: StatutModel
                });
                if (!myUser) {
                    return buildResponse(404, {
                        message: "User not found"
                    })
                }
                // Get statut
                statutId = myUser.Statut.id;
                userStatut = await StatutModel.findOne({
                    where: {
                        id: statutId
                    }
                })
                if (status == "accepted") {
                    userStatut.set({
                        val_statut: 'accepté',
                        argument: "aucun"
                    })
                    userStatut.save();

                    //*** Sending success email ***
                    console.log("Sending email")
                    await sendEmail(myUser.email, "Bienvenue chez Autorun",
                        getEmailBody(myUser.nom + " " + myUser.prenom, true)
                        )

                    return buildResponse(200, {
                        message: 'Locataire accepted'
                    })
                }
                if (status == "refused") {
                    body = JSON.parse(event.body)

                    // Validate if body has argument
                    if (!body.argument)
                        return buildResponse(400, {
                            message: "Missing argument"
                        })

                    userStatut.set({
                        val_statut: 'refusé',
                        argument: body.argument
                    })
                    userStatut.save();

                    //*** Sending success email ***
                    await sendEmail(myUser.email, "A propos de votre inscription",
                        getEmailBody(myUser.nom + " " + myUser.prenom, false, body.argument)
                    )

                    return buildResponse(200, {
                        message: 'Locataire refused'
                    })
                }
                buildResponse(400, {
                    message: 'Unknown status'
                })
                break;
        }

    } catch (err) {
        console.log(err);
        return buildResponse(502, err);
    }
};
