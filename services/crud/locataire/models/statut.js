let {Sequelize, DataTypes} = require('sequelize');
const db = require('/opt/nodejs/database')
const user = require('./user.js')

var statut = db.define('Statut', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'statut_id',
        references: {
            model: user,
            column: 'statut_utilisateur'
        }
    },
    val_statut: {
        type: DataTypes.STRING,
        field: 'val_statut'
    },
    argument: {
        type: DataTypes.STRING,
        field: 'argument'
    }
}, {
    freezeTableName: true,
    timestamps: false,
});
module.exports = statut;