let {Sequelize, DataTypes} = require('sequelize');
const db = require('/opt/nodejs/database');
const User = require('./user.js');

var auth = db.define('Authorization', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'authorization_id',
        references: {
            model: User,
            column: 'id'
        }
    },
    email: {
        type: DataTypes.STRING,
        field: 'email'
    },
    mdp: {
        type: DataTypes.STRING,
        field: 'mdp'
    },
    access_token: {
        type: DataTypes.STRING,
        field: 'access_token'
    },
    reset_pwd_token: {
        type: DataTypes.STRING,
        field: 'access_token'
    }
}, {
    freezeTableName: true // Model tableName will be the same as the model name
});

module.exports = auth;