const UtilisateurModel = require('models/user');
const ATCModel = require('models/atc');
const Utils = require('/opt/nodejs/utils');
const validator = require('validator')

let response;

function buildResponse(responseCode, body) {
    return {
        'statusCode': responseCode,
        'body': JSON.stringify(body)
    }
}

function validateLocataireSignup(body) {
    try {
        return validator.isEmail(body.email)
            && validator.isAlpha(body.nom)
            && validator.isLength(body.nom, {min: 3})
            && validator.isAlpha(body.prenom)
            && validator.isLength(body.prenom, {min: 3})
            && validator.isNumeric(body.num_tel)
            && validator.isLength(body.num_tel, {min: 9})
    } catch (e) {
        return false
    }
}

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */
exports.lambdaHandler = async (event, context) => {
    const {method} = event.requestContext.http;

    // Defining models associations
    UtilisateurModel.hasOne(ATCModel, {
        foreignKey: 'atc_id'
    });


    let body;
    let myUser = null;
    let userStaut = null;

    try {
        switch (method) {
            case "GET":
                let {id} = event.pathParameters;
                if (id) {// Return a certain ATC

                    const user = await UtilisateurModel.findOne({
                        where: {
                            id: id,
                            type_utilisateur: "atc"
                        },
                        include: [ATCModel]
                    });
                    if (!user) {
                        return buildResponse(404, {
                            message: "User not found"
                        })
                    }
                    return buildResponse(200, {
                        user: user,
                    });
                } else {
                    // return all ATCs
                    const users = await UtilisateurModel.findAll({
                        where: {
                            type_utilisateur: "atc"
                        },
                        include: [ATCModel]});
                    if (!users) {
                        return buildResponse(404, {
                            message: "User not found"
                        })
                    }
                    console.log(users)
                    return buildResponse(200, users);
                }
                break;
            case "POST":
                // Validate body
                body = JSON.parse(event.body);
                if (!validateLocataireSignup(body)) {
                    return buildResponse(400, {
                        message: "Wrong input payload"
                    })
                }

                // Getting user
                let user =  await UtilisateurModel.findOne({
                    where: {
                        email: body.email,
                        type_utilisateur: "atc"
                    },
                    include: [ATCModel]
                });
                if (user) {
                    return buildResponse(403, {
                        message: "ATC exists already"
                    })
                }

                hashedPassword = Utils.setPassword(body.mdp);

                user = await UtilisateurModel.create({
                    ...body,
                    mdp: hashedPassword,
                    type_utilisateur: "atc"
                });

                // Creating a ATC
                await ATCModel.create({
                    id: user.id,
                })

                return buildResponse(200, {
                    message: "User created successfully",
                    user: user
                })
            case "PUT":
                body = JSON.parse(event.body)
                // Validate body
                // TODO make ATC update validator
                // if (!validateLocataireSignup(body)) {
                //     return buildResponse(400, {
                //         message: "Wrong input"
                //     })
                // }

                let idATC = event.pathParameters.id;

                let atc =  await UtilisateurModel.findOne({
                    where: {
                        id: idATC,
                        type_utilisateur: "atc"
                    },
                    include: [ATCModel]
                })
                atc.set({
                    ...body
                })
                atc.save();
                return buildResponse(200, {
                    message:" User updated",
                    user: atc
                })
                break;
            case "DELETE":
                let myId = event.pathParameters.id;
                if (myId) {

                    const user = await UtilisateurModel.findOne({
                        where: {
                            id: myId,
                            type_utilisateur: "atc"
                        },
                        include: [ATCModel]
                    });
                    if (!user) {
                        return buildResponse(404, {
                            message: "User not found"
                        })
                    }
                    await user.destroy({
                        include: [ATCModel]
                    });
                    return buildResponse(200, {
                        message: "User Deleted"
                    })
                }
                break;
        }

    } catch (err) {
        console.log(err);
        return buildResponse(502, err);
    }
};
