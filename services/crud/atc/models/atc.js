let {Sequelize, DataTypes} = require('sequelize');
const db = require('/opt/nodejs/database')
const user = require('./user')

var atc = db.define('ATC', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'atc_id',
        references: {
            model: user,
            column: 'utilisateur_id'
        }
    }
}, {
    freezeTableName: true,
    timestamps: false // Model tableName will be the same as the model name
});

module.exports = atc;